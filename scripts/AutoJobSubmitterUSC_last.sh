#!/bin/sh

#################################
#####  GET CPU USAGE
#################################
function GetCPUUsage() {
                    
	local CPUId=$1
	local MonitoringTime=$2

	local CPUPercent=-1

	#####################################
	####   START
	#####################################
	## print command output
	echo ""
	echo "**********************************************"
	echo "******   Top CPU-consuming processes  ********"
	echo "**********************************************"
	ps -eo pcpu,pid,user,args | sort -k 1 -r | head -10
	echo "**********************************************"

	#echo ""
	echo ""

	## print command output
	#echo "*** CPU-usage list ***"
	#cat /proc/stat | grep cpu$CPUId

	#echo ""
	#echo ""

	local cpu=`cat /proc/stat | grep cpu$CPUId | awk '{print $1}'`
	local totalUser_start=`cat /proc/stat| grep cpu$CPUId | awk '{print $2}'`
	local totalUserLow_start=`cat /proc/stat| grep cpu$CPUId | awk '{print $3}'`
	local totalSys_start=`cat /proc/stat| grep cpu$CPUId | awk '{print $4}'`
	local totalIdle_start=`cat /proc/stat| grep cpu$CPUId | awk '{print $5}'`
	
	## sleep a MonitoringTime and then calculate again load
	sleepCommand="sleep $MonitoringTime"'s'
	echo "Sleeping...$sleepCommand"
	eval $sleepCommand

	#####################################
	####   END
	#####################################
	## print command output
	#echo "*** Top CPU-consuming processes ***"
	#ps -eo pcpu,pid,user,args | sort -k 1 -r | head -10

	#echo ""
	#echo ""

	## print command output
	#echo "*** CPU-usage list ***"
	#cat /proc/stat | grep cpu$CPUId

	#echo ""
	#echo ""
	
	local totalUser_end=`cat /proc/stat| grep cpu$CPUId | awk '{print $2}'`
	local totalUserLow_end=`cat /proc/stat| grep cpu$CPUId | awk '{print $3}'`
	local totalSys_end=`cat /proc/stat| grep cpu$CPUId | awk '{print $4}'`
	local totalIdle_end=`cat /proc/stat| grep cpu$CPUId | awk '{print $5}'`


	if [ $totalUser_end -lt $totalUser_start ] || [ $totalUserLow_end -lt $totalUserLow_start ] || [ $totalSys_end -lt $totalSys_start ] || [ $totalIdle_end -lt $totalIdle_start ]; then
		## Overflow detection. Just skip this value.
    CPUPercent=-1;
	else
		## Calculate CPU usage
		(( usage= $totalUser_end - $totalUser_start + $totalUserLow_end - $totalUserLow_start + $totalSys_end - $totalSys_start ))
		(( total= $usage + $totalIdle_end - $totalIdle_start ))
		(( CPUPercent= 100 * $usage / $total ))
	fi

	echo $CPUPercent;
}
# close function




#################################
#####  MAIN
#################################
if [ "$1" = "" ]; then 
    echo "Usage: $0 nMaxJobsPerNode [nMaxRecFiles]"
    echo "  1) nMaxJobsPerNode    Max number of jobs per node (<=8)"
		echo "                        ...but don't choose 8 or guys will kill you!"
    echo "  2) nMaxRecFiles       Max number of files in the list to be reconstructed"
    exit 0
fi


if [ "$1" = "" ]; then
	echo "ERROR: Missing nMaxJobsPerNode --> Please specify it as argument!"
  exit 0
else
  nMaxJobsPerNode=$1
fi

if [ "$nMaxJobsPerNode" -gt "8" ]; then
	echo "ERROR: nMaxJobsPerNode too large!"
  exit 0
fi

if [ "$2" = "" ]; then
	echo "INFO: Reconstructing all files in the list!"
else
  nMaxRecFiles=$2
fi

node_list=(20 30 32)
###node_list=(12 13 14 15 16 17 18 19 20 21 22 23 24 26 27 28 29 30 31 32 33 34 35)
nNodes=${#node_list[@]}
nCPUPerNode=8 
cpuMonitoringTime=5  ## in seconds
cpuUsageThreshold=40 ## in %

echo "Number of nodes where to submit is $nNodes"

## initialize to first node in the list
currentNode=${node_list[0]}
node_counter=0
file_counter=0
nFreeSlots=0

## command to count nSubmittedJobs online
##nCurrentJobs=`pgrep <processName> -u riggi | wc -l`

## display number of top-cpu-consuming processes (only users not root)
##ps -NU root -o pcpu,pid,user,args | sort -k 1 -r | head -10


while read filename
do   

	node_counter=0

	## dummy check
	if [ $nFreeSlots -lt 0 ]; then 
		echo "ERROR: nFreeSlots is negative!...exit"
		break;
	fi

	## If no slots are available, find one
	while [ $nFreeSlots -le 0 ]
	do
		echo "Searching free slots in nodes"
		nohup ssh simone.riggi@nodo0$node_counter.inv.usc.es exec $PWD/NodeSelector.sh  >& $PWD/NodeStatus.log &



		## Loop over all cpu and find free slots
		for ((cpuNo=1; cpuNo<=$nCPUPerNode; cpuNo=$cpuNo+1))
		do
			(( cpuId= $cpuNo - 1 ))
			
			##cpuUsage=$(GetCPUUsage $cpuId $cpuMonitoringTime) 
			if [ $cpuNo -eq 1 ]; then 
				echo ""
				echo "**********************************************"
				echo "******   Top CPU-consuming processes  ********"
				echo "**********************************************"
				ps -eo pcpu,pid,user,args | sort -k 1 -r | head -10
				echo "**********************************************"
				echo ""
			fi

			cpu=`cat /proc/stat | grep cpu$cpuId | awk '{print $1}'`
			totalUser_start=`cat /proc/stat| grep cpu$cpuId | awk '{print $2}'`
			totalUserLow_start=`cat /proc/stat| grep cpu$cpuId | awk '{print $3}'`
			totalSys_start=`cat /proc/stat| grep cpu$cpuId | awk '{print $4}'`
			totalIdle_start=`cat /proc/stat| grep cpu$cpuId | awk '{print $5}'`
	
			## sleep a MonitoringTime and then calculate again load
			sleepCommand="sleep $cpuMonitoringTime"'s'
			echo "Sleeping...$sleepCommand"
			eval $sleepCommand

			totalUser_end=`cat /proc/stat| grep cpu$cpuId | awk '{print $2}'`
			totalUserLow_end=`cat /proc/stat| grep cpu$cpuId | awk '{print $3}'`
			totalSys_end=`cat /proc/stat| grep cpu$cpuId | awk '{print $4}'`
			totalIdle_end=`cat /proc/stat| grep cpu$cpuId | awk '{print $5}'`

			if [ $totalUser_end -lt $totalUser_start ] ; then
				## Overflow detection. Just skip this value.
    		CPUPercent=-1;	
			elif [ $totalUserLow_end -lt $totalUserLow_start ]
			then
				## Overflow detection. Just skip this value.
    		CPUPercent=-1;
			elif [ $totalSys_end -lt $totalSys_start ]
			then
				## Overflow detection. Just skip this value.
    		CPUPercent=-1;
			elif [ $totalIdle_end -lt $totalIdle_start ]
			then
				## Overflow detection. Just skip this value.
    		CPUPercent=-1;
			else
				## Calculate CPU usage
				(( usage= $totalUser_end - $totalUser_start + $totalUserLow_end - $totalUserLow_start + $totalSys_end - $totalSys_start ))
				(( total= $usage + $totalIdle_end - $totalIdle_start ))
				(( CPUPercent= 100 * $usage / $total ))
			fi

			cpuUsage=$CPUPercent
			echo "** Node $node_counter: CPU$cpuId Load= $cpuUsage% **"

			if [ $cpuUsage -lt $cpuUsageThreshold ] ; then
				(( nFreeSlots= $nFreeSlots + 1 ))	
			fi
		done

		
		if [ $nFreeSlots -eq 0 ] ; then
			## if all nodes have been searched
			## revert to first node and sleep before restarting
			## otherwise increment node
			if [ $node_counter -eq $nNodes ] ; then		
				node_counter=0
				sleep 10m
			else
				(( node_counter= $node_counter + 1 ))	
			fi	
		else
			submitNode=${node_list[$node_counter]}

			## if nFreeSlots exceed maximum allowed number of jobs per node
			## reset to such number
			if [ $nFreeSlots -gt $nMaxJobsPerNode ] ; then
				nFreeSlots=$nMaxJobsPerNode
			fi

			break;
		fi		

	done
	## end search free slots

	## Submit job in the free node
	echo "Reconstruct file $filename in node $submitNode"
	##sh JobSubmitterUSCNode.sh 1 1 1 1 1 $filename 2 $submitNode

	## Reduce nFreeSlots and increment file_counter
	(( nFreeSlots= $nFreeSlots - 1 ))	
	(( file_counter= $file_counter + 1 ))	


	## If total number of jobs exceeds the maximum 
	## stop everything!
	if [ "$nMaxRecFiles" != "" ] && [ $file_counter -ge $nMaxRecFiles ]; then 
		break;
	fi

	
done < ProtonLibFileList.dat 
## close loop over files


echo "*** END SCRIPT ***"

