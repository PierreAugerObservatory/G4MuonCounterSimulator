#!/bin/tcsh

echo nodo$1 cpu$2 
source ~/setvars.sh

mkdir /scratch/gonzalo
cd /scratch/gonzalo
rm -rf nodo$1/cpu$2

mkdir nodo$1
cd nodo$1

mkdir cpu$2
cd cpu$2

cp -r /home/gonzalo.rodriguez/SdSimAndHasRecSim/* .
cp run_dir/RunOffline.sh .
scp gonzalo.rodriguez@igfaecr1.usc.es:/data1/Showers/ShLib6/RandomThetaPhi/3.18/pq_3.18_Rn_$3.grdpcles .
scp gonzalo.rodriguez@igfaecr1.usc.es:/data1/Showers/ShLib6/RandomThetaPhi/3.18/pq_3.18_Rn_$3.t2207 .
scp gonzalo.rodriguez@igfaecr1.usc.es:/data1/Showers/ShLib6/RandomThetaPhi/3.18/pq_3.18_Rn_$3.t2707 .
scp gonzalo.rodriguez@igfaecr1.usc.es:/data1/Showers/ShLib6/RandomThetaPhi/3.18/pq_3.18_Rn_$3.t5207 .
scp gonzalo.rodriguez@igfaecr1.usc.es:/data1/Showers/ShLib6/RandomThetaPhi/3.18/pq_3.18_Rn_$3.t5511 .

./WriteEventFileReadXML.pl pq_3.18_Rn_$3.grdpcles
./RunOffline.sh pq_3.18_Rn_$3.grdpcles > & out2.dat &






