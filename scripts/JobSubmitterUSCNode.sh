#!/bin/sh

if [ "$2" = "" ]; then 
    echo "Usage: $0 begin end [step]"
    echo "  1) begin     The first value of the sequence"
    echo "  2) end       The last value of the sequence"
    echo "  3) step      The step between two submissions"
    echo "  4) Number of events to be reconstructed in the node"
    echo "  5) Number of events looped in the Module Sequence (suggested 1) "
    echo "  6) Name of file CONEX to be reconstructed (without .root extension)"
    echo "  7) First MySQL server to be queried...and to be probably killed first!"
    echo "        1: cc01.ct.infn.it"
    echo "        2: myserver 192.84.150.137"
    echo "        3: pcauger  192.167.0.118"	
    echo "  8) USC NODE (12-34)"
    #qstat -q
		#qstat -f		

    exit 0
fi

joblist="jobs.list"

begin_index=$1
end_index=$2

if [ "$3" = "" ]; then
    step=1;
else
    step=$3
fi


if [ "$4" = "" ]; then
    Nev=1;
else
    Nev=$4
fi

if [ "$5" = "" ]; then
    Nev_modulesequence=1; 
else
    Nev_modulesequence=$5 
fi

if [ "$6" = "" ]; then
    echo "ERROR: Missing input file name --> Please specify it!"
    exit 0
else
    SIMFILENAME=$6 
fi

if [ "$7" = "" ]; then
    echo "ERROR: Missing MySQL server flag --> Please specify it!"
    exit 0
else
    MYSQLSERVER=$7
fi

if [ "$8" = "" ]; then
    echo "ERROR: Missing USC NODE --> Please specify it!"
    exit 0
else
    NODE=$8
fi

  export BASEDIR=$PWD
  cd $BASEDIR

  echo ""
  echo "*** Untar the job template ***"
  tar -xzvf $BASEDIR/OfflineJobTemplate.tar.gz
  cp -rp OfflineJobTemplate MyOfflineJobTemplate
  
  echo ""
  echo "*** Enter in the job template directory ***"
  cd $BASEDIR/MyOfflineJobTemplate
  
  echo ""
  ##echo "*** Cleaning all ***"
  ##make clean
  
  echo ""
  echo "*** Creating all needed XML files via MakeXMLFiles.sh script ***"  
  cd $BASEDIR/MyOfflineJobTemplate/scripts
  $BASEDIR/MyOfflineJobTemplate/scripts/MakeXMLFiles.sh $SIMFILENAME.part $Nev_modulesequence $MYSQLSERVER
  echo "** Move XMLs in xml directory ***"
  mv *.xml *.xml.in $BASEDIR/MyOfflineJobTemplate/xml
  
  echo ""
  ##echo "*** Invoking compilation of template ***"
  ##cd $BASEDIR/MyOfflineJobTemplate
  ##make
  
  echo ""
  echo "*** Create a new template to be used for the jobs ***"
  cd $BASEDIR
  tar -czf MyOfflineJobTemplate.tar.gz MyOfflineJobTemplate




for ((index=$begin_index; index<=$end_index; index=$index+$step))
  do
  
  echo ""
  echo "*** Untar the job template ***"
  tar -xzvf $BASEDIR/MyOfflineJobTemplate.tar.gz
  
  
  joboutdir='job_'"$SIMFILENAME"'_RUN'"$index"
  mv MyOfflineJobTemplate $joboutdir
  export CURRENTJOBDIR=$BASEDIR/$joboutdir
  
  #adstoutfile='ADST_'"$SIMFILENAME"'-JOB'"$index"
  #echo $adstoutfile
  siminfooutfile='SimInfo_'"$SIMFILENAME"'-RUN'"$index"
  echo $siminfooutfile
  
	simoutfile='Output_'"$SIMFILENAME"'-RUN'"$index"
  echo $simoutfile
  
	

  echo ""
	

shfile="OfflineRun$index.sh"
echo "*** Creating sh file $shfile ***"
( 
      
      #echo "#PBS -o $BASEDIR"
      #echo "#PBS -o $BASEDIR"
      #echo '#PBS -r n'          
      #echo '#PBS -S /bin/sh'
      #echo "#PBS -N job$index"
      #echo '#PBS -M simone.riggi@ct.infn.it'
      #echo '#PBS -m be'
      #echo '#PBS -p 1'
      
			echo '# Use current working directory'
			echo '#$ -cwd'
			echo '# Join stdout and stderr'
			echo '#$ -j y'
			echo '# Set job name'
			echo "#$ -N job$index"
			echo '# Run job through bash shell'
			echo '#$ -S /bin/sh'		
			echo '# Send beginning and end job info to the specified email address'
			echo '#$ -M simone.riggi@ct.infn.it'
			echo '#$ -m b e'
			echo '# Set job priority'
			echo '#$ -p 1'
      echo " "

      echo " "

      echo 'echo "*************************************************"'
      echo 'echo "****         PREPARE JOB                     ****"'
      echo 'echo "*************************************************"'

      echo 'echo ""'
      
      
      echo " "

      echo 'echo ""'
      echo 'echo "*** Source the software environment ***"'
      #echo 'source /home/farmcc/riggism/setvars_Offline.sh'
      echo 'source /home/simone.riggi/Software/setvars_packages.sh'

      echo 'echo ""'
      
      echo "export JOBDIR=$CURRENTJOBDIR"
			echo 'export JOBOBJDIR=$JOBDIR/obj'
      echo 'export JOBINPUTDIR=$JOBDIR/inputFiles'
      echo 'export JOBOUTPUTDIR=$JOBDIR/outputFiles'
      echo 'export JOBXMLDIR=$JOBDIR/xml'
      echo 'export JOBSCRIPTDIR=$JOBDIR/scripts'
      echo 'export JOBMYSQLDIR=$JOBDIR/mysql'     
     
      echo " "

      echo " "

      echo 'echo "*************************************************"'
      echo 'echo "****         PRINT JOB QUEUE INFO            ****"'
      echo 'echo "*************************************************"'
			echo 'echo "Current jobId: $JOB_ID"'
			echo 'echo "Current jobName: $JOB_NAME"'
			echo 'echo "Current hostname: $HOSTNAME"'
			echo 'echo "Current queue: $QUEUE"' 
      echo 'echo ""'
      

      echo 'echo ""'

      echo " "

      echo " "
      echo 'echo "*************************************************"'
      echo 'echo "****         RUN SIMULATION                  ****"'
      echo 'echo "*************************************************"'
      echo 'echo ""'
      echo 'i=1' 
      echo 'GoodEventCounter=0'
      echo 'while [ $i -le '"$Nev ]"
      echo 'do'
      echo '  echo "*** Executing the Offline run: EvNo --> $i ***"'
      echo '  cd $JOBDIR'
      
      
      echo '  $JOBDIR/userAugerOffline -b xml/bootstrap.xml'
      echo '  echo ""'

      
      echo '  echo "*** Check if event is reconstructed: check if output file exists ***"'
      echo '  OUTPUTFILE=`ls -d Output*`'
      echo '  if [ $OUTPUTFILE = "Output.root" ]'
      echo '  then'
      echo '    mv Output.root $JOBOUTPUTDIR/'"$simoutfile"'_$i.root'
      echo '    i=$((i+1))'
      echo '    GoodEventCounter=$((GoodEventCounter+1))'
      echo '  else'
      echo '    echo "*** No output produced...continue with the external simulation loop ***"'
      echo '    i=$((i+1))'
      echo '  fi'
			
      echo '  cd $JOBDIR'
      echo 'done'   

      echo " "
      echo " "
      
      echo 'echo ""'  
      echo 'echo ""'
      echo 'echo "*************************************************"'
      echo 'echo "****         JOB SUMMARY                     ****"'
      echo 'echo "*************************************************"'
      echo 'i=$((i-1))'
      echo 'echo "Number of generated events: $i"'
      echo 'echo "Number of reconstructed events: $GoodEventCounter"' 
      echo 'echo ""'
      echo " "


      echo 'echo "*** END RUN ***"'

 ) > $shfile

chmod +x $shfile

mv $shfile $CURRENTJOBDIR

# submits the job to USC node
##qsub -q $PBSQUEUE $CURRENTJOBDIR/$shfile
nohup ssh simone.riggi@nodo0$NODE.inv.usc.es exec $CURRENTJOBDIR/$shfile >& $CURRENTJOBDIR/out.log &
#$CURRENTJOBDIR/$shfile


done

rm -rf $BASEDIR/MyOfflineJobTemplate.tar.gz



