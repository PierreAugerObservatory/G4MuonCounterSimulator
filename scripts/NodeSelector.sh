#!/bin/sh

node=$1
nCPUPerNode=$2
cpuUsageThreshold=$3
nMaxJobsPerNode=$4
cpuMonitoringTime=$5


## nJobsSubmitted in this node
nSubmittedJobs=`pgrep userAugerOffline -u simone.riggi | wc -l`

if [ $nSubmittedJobs -ge $nMaxJobsPerNode ] ; then
	echo "node	$node	nFreeSlots	0"
	exit 0			
fi

## Loop over all cpu and find free slots
for ((cpuNo=1; cpuNo<=$nCPUPerNode; cpuNo=$cpuNo+1))
do
		(( cpuId= $cpuNo - 1 ))
			
		if [ $cpuNo -eq 1 ]; then 
			echo ""
			echo "**********************************************"
			echo "******   Top CPU-consuming processes  ********"
			echo "**********************************************"
			ps -eo pcpu,pid,user,args | sort -k 1 -r | head -10
			echo "**********************************************"
			echo ""
		fi

		cpu=`cat /proc/stat | grep cpu$cpuId | awk '{print $1}'`
		totalUser_start=`cat /proc/stat| grep cpu$cpuId | awk '{print $2}'`
		totalUserLow_start=`cat /proc/stat| grep cpu$cpuId | awk '{print $3}'`
		totalSys_start=`cat /proc/stat| grep cpu$cpuId | awk '{print $4}'`
		totalIdle_start=`cat /proc/stat| grep cpu$cpuId | awk '{print $5}'`
	
		## sleep a MonitoringTime and then calculate again load
		sleepCommand="sleep $cpuMonitoringTime"'s'
		echo "Sleeping...$sleepCommand"
		eval $sleepCommand

		totalUser_end=`cat /proc/stat| grep cpu$cpuId | awk '{print $2}'`
		totalUserLow_end=`cat /proc/stat| grep cpu$cpuId | awk '{print $3}'`
		totalSys_end=`cat /proc/stat| grep cpu$cpuId | awk '{print $4}'`
		totalIdle_end=`cat /proc/stat| grep cpu$cpuId | awk '{print $5}'`

		if [ $totalUser_end -lt $totalUser_start ] || [ $totalUserLow_end -lt $totalUserLow_start ] || [ $totalSys_end -lt $totalSys_start ] || [ $totalIdle_end -lt $totalIdle_start ] ; then
			## Overflow detection. Just skip this value.
    	CPUPercent=-1;	
		else
			## Calculate CPU usage
			(( usage= $totalUser_end - $totalUser_start + $totalUserLow_end - $totalUserLow_start + $totalSys_end - $totalSys_start ))
			(( total= $usage + $totalIdle_end - $totalIdle_start ))
			if [ $total -gt 0 ] ; then
				(( CPUPercent= 100 * $usage / $total ))
			else
				CPUPercent=0
			fi
		fi

		cpuUsage=$CPUPercent
		echo "** Node $node: CPU$cpuId Load= $cpuUsage% **"

		if [ $cpuUsage -lt $cpuUsageThreshold ] ; then
				(( nFreeSlots= $nFreeSlots + 1 ))	
		fi
done


(( nAvailableSlots= $nMaxJobsPerNode - $nSubmittedJobs ))	


if [ $nAvailableSlots -lt $nFreeSlots ] ; then
	nFreeSlots=$nAvailableSlots
fi
	
echo "node	$node	nFreeSlots	$nFreeSlots"

