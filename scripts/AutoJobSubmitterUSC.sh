
#!/bin/sh

if [ "$1" = "" ]; then
	echo "ERROR: Missing nMaxJobsPerNode --> Please specify it as argument!"
  exit 0
else
  nMaxJobsPerNode=$1
fi

if [ "$nMaxJobsPerNode" -gt "8" ]; then
	echo "ERROR: nMaxJobsPerNode too large!"
  exit 0
fi

if [ "$2" = "" ]; then
	echo "INFO: Reconstructing all files in the list!"
else
  nMaxRecFiles=$2
fi

node_list=(20 30 32)
###node_list=(12 13 14 15 16 17 18 19 20 21 22 23 24 26 27 28 29 30 31 32 33 34 35)
nNodes=${#node_list[@]}
echo "Number of nodes where to submit is $nNodes"

## initialize to first node in the list
currentNode=${node_list[0]}
counter=0
ncounter=1
file_counter=0

## command to count nSubmittedJobs online
nCurrentJobs=`pgrep <processName> -u riggi | wc -l`

## display number of top-cpu-consuming processes (only users not root)
ps -NU root -o pcpu,pid,user,args | sort -k 1 -r | head -10


while read filename
do   
  (( counter= $counter + 1 ))
	(( file_counter= $file_counter + 1 ))

	echo "Reconstruct file $filename in node $currentNode"
	sh JobSubmitterUSCNode.sh 1 1 1 1 1 $filename 2 $currentNode
   
	## if total number of jobs exceeds the maximum 
	## stop everything!
	if [ "$file_counter" = "$nMaxRecFiles" ]; then 
		break;
	fi

	## if max number of jobs per node has been reached
	## revert to the following node in the list
	## reset job counter  	  
  if [ "$counter" = "$nMaxJobsPerNode" ]; then 			
		if [ "$ncounter" = "$nNodes" ]; then
			ncounter=1
			currentNode=${node_list[0]}
			echo ""
			echo "Sleeping 10 minutes..."
			sleep 10m	
		else
			currentNode=${node_list[$ncounter]}
			echo ""
		fi   
	  
		counter=0;
		(( ncounter= $ncounter + 1 ))
  fi


done < ProtonLibFileList.dat ## close loop over files


echo "*** END SCRIPT ***"

