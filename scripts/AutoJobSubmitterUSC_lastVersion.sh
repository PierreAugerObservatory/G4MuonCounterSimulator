#!/bin/sh


#################################
#####  MAIN
#################################
if [ "$1" = "" ]; then 
    echo "Usage: $0 nMaxJobsPerNode [nMaxRecFiles]"
    echo "  1) nMaxJobsPerNode    Max number of jobs per node (<=8)"
		echo "                        ...but don't choose 8 or guys will kill you!"
    echo "  2) nMaxRecFiles       Max number of files in the list to be reconstructed"
    exit 0
fi


if [ "$1" = "" ]; then
	echo "ERROR: Missing nMaxJobsPerNode --> Please specify it as argument!"
  exit 0
else
  nMaxJobsPerNode=$1
fi

if [ "$nMaxJobsPerNode" -gt "8" ]; then
	echo "ERROR: nMaxJobsPerNode too large!"
  exit 0
fi

if [ "$2" = "" ]; then
	echo "INFO: Reconstructing all files in the list!"
else
  nMaxRecFiles=$2
fi

node_list=(20 30 32)
###node_list=(12 13 14 15 16 17 18 19 20 21 22 23 24 26 27 28 29 30 31 32 33 34 35)
nNodes=${#node_list[@]}
(( lastNodeIndex= $nNodes - 1 ))
nCPUPerNode=8 
cpuMonitoringTime=2  ## in seconds
cpuUsageThreshold=40 ## in %

echo "Number of nodes where to submit is $nNodes"

## initialize to first node in the list
node=${node_list[0]}
node_counter=0
file_counter=0
nFreeSlots=0



## command to count nSubmittedJobs online
##nCurrentJobs=`pgrep <processName> -u riggi | wc -l`

## display number of top-cpu-consuming processes (only users not root)
##ps -NU root -o pcpu,pid,user,args | sort -k 1 -r | head -10

CURRENTDIR=$PWD


while read filename
do   

	echo "filename $filename"
	

	## dummy check
	if [ $nFreeSlots -lt 0 ]; then 
		echo "ERROR: nFreeSlots is negative!...exit"
		break;
	fi

	## If no slots are available, find one
	while [ $nFreeSlots -le 0 ]
	do
		echo "Searching free slots in node $node"
		
		remoteCommand="ssh simone.riggi@nodo0$node.inv.usc.es"
		scriptCommand="$CURRENTDIR/NodeSelector.sh $node_counter $nCPUPerNode $cpuUsageThreshold $nMaxJobsPerNode $cpuMonitoringTime"
		nFreeSlots="$($remoteCommand $scriptCommand < /dev/null | grep nFreeSlots | awk '{ print $4}')"
		echo "nFreeSlots= $nFreeSlots"

		
		if [ $nFreeSlots -eq 0 ] ; then
			## if all nodes have been searched
			## revert to first node and sleep before restarting
			## otherwise increment node
			## if [ $node_counter -eq $nNodes ] ; then	
			if [ $node_counter -eq $lastNodeIndex ] ; then		
				node_counter=0
				node=${node_list[$node_counter]}	
				sleep 10m
			else
				(( node_counter= $node_counter + 1 ))
				node=${node_list[$node_counter]}	
			fi	
		else
			submitNode=${node_list[$node_counter]}
			echo "found node $submitNode where to submit...break loop"			
			break;
		fi		

	done
	
	## end search free slots

	## Submit job in the free node
	echo "Reconstruct file $filename in node $submitNode"
	sh $CURRENTDIR/JobSubmitterUSCNode.sh 1 1 1 1 1 $filename 2 $submitNode

	## Reduce nFreeSlots and increment file_counter
	(( nFreeSlots= $nFreeSlots - 1 ))	
	(( file_counter= $file_counter + 1 ))	
	echo "nFreeSlots= $nFreeSlots"
	#echo "file_counter= $file_counter"

	## If total number of jobs exceeds the maximum 
	## stop everything!
	if [ "$nMaxRecFiles" != "" ] && [ $file_counter -ge $nMaxRecFiles ]; then
		echo "Maximum processed files reached...exit loop" 
		break;
	fi

done < "$CURRENTDIR/ProtonLibFileList.dat"

## close loop over files


echo "*** END SCRIPT ***"

