#!/bin/sh

export WORKDIR='$pwd'

####################################################################
##       SCRIPT ARGs
####################################################################
#  $1 Simulated input filename (CONEX,CORSIKA,AIRES) 
#  $2 Number of events to be reconstructed (if missing, set to 1)
#  $3 Which MySQL server to use

if [ "$1" = "" ]; then
    echo "ERROR: Missing input simulated file"
    echo "Specify the input filename to put in the EventReader"
    exit 0
else 
    SIMFILENAME=$1
fi

if [ "$2" = "" ]; then
    NEV=1;
elif [ "$2" = "all" ]; then
    NEV="unbounded"
else
    NEV=$2
fi

if [ "$3" = "" ]; then
    echo "ERROR: Missing MySQL server flag"
    exit 0
else
    MYSQLSERVER=$3
fi


#########################################################
#  CREATE NEEDED xml.in to be extended in XML       
########################################################

#BOOTSTRAP
##bootstrapfile="bootstrap.xml.in"
bootstrapfile="bootstrap.xml"
echo "Creating bootstrap file $bootstrapfile"
( 
    echo '<?xml version="1.0" encoding="iso-8859-1"?>'
    echo ""
    echo '<!DOCTYPE bootstrap ['
    echo '<!-- alias for default FD config files -->'
    ##echo "<!ENTITY defaultFDConfig SYSTEM '@CONFIGDIR@/exampleFDConfig.xml'>"
		#echo "<!ENTITY defaultFDConfig SYSTEM '""$AUGEROFFLINEROOT/share/auger-offline/config/exampleFDConfig.xml'"">"
    echo '<!-- alias for default SD config files -->'
    ##echo "<!ENTITY defaultSDConfig SYSTEM '@CONFIGDIR@/exampleSDConfig.xml'>"
		echo "<!ENTITY defaultSDConfig SYSTEM '""$AUGEROFFLINEROOT/share/auger-offline/config/exampleSDConfig.xml'"">"
    echo ']>'

    echo ""

    echo '<bootstrap xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'
    ##echo "xsi:noNamespaceSchemaLocation='@SCHEMALOCATION@/bootstrap.xsd'"
		echo "xsi:noNamespaceSchemaLocation='""$AUGEROFFLINEROOT/share/auger-offline/config/bootstrap.xsd'"
    echo 'xmlns:xlink="http://www.auger.org/schema/types">'

    echo ""

    ##echo '<!-- get default configuration for FD -->'
    ##echo '&defaultFDConfig;'
    echo '<!-- get default configuration for SD -->'
    echo '&defaultSDConfig;'
    echo ""
    echo '<centralConfig>'

    echo ""
    echo '<configLink'
    echo 'id         = "ModuleSequence"'
    echo 'type       = "XML"'
    echo 'xlink:href = "./xml/ModuleSequence.xml" />'
    
    echo ""
    echo '<configLink'
    echo 'id         = "EventFileReader"'
    echo 'type       = "XML"'
    echo 'xlink:href = "./xml/EventFileReader.xml" />'
    echo ""

    echo '<configLink'
    echo 'id         = "EventGenerator"'
    echo 'type       = "XML"'
    echo 'xlink:href = "./xml/EventGenerator.xml" />'
    echo ""

    echo '<configLink'
    echo '  id         = "ShowerRegenerator"'
    echo '  type       = "XML"'
    echo '  xlink:href = "./xml/ShowerRegenerator.xml"/>'
 
		echo '<configLink'
    echo '  id         = "G4MuonCounterSimulatorUSC"'
    echo '  type       = "XML"'
    echo '  xlink:href = "./xml/G4MuonCounterSimulator.xml"/>'

		echo '<configLink'
    echo '  id         = "G4MuonCounterReconstructorUSC"'
    echo '  type       = "XML"'
    echo '  xlink:href = "./xml/G4MuonCounterReconstructor.xml"/>'

		echo '<configLink'
    echo '  id         = "G4MuonCounterParticleInjector"'
    echo '  type       = "XML"'
    echo '  xlink:href = "./xml/G4MuonCounterParticleInjector.xml"/>'

		echo '<configLink'
    echo '  id         = "EventFileRecorder"'
    echo '  type       = "XML"'
    echo '  xlink:href = "./xml/EventFileRecorder.xml"/>'

    echo '<configLink'
    echo 'id         = "SManagerRegister"'
    echo 'type       = "XML"'
    echo 'xlink:href = "./xml/SManagerRegisterConfig.xml" />'
    echo ""

		echo '<configLink'
    echo 'id         = "SStationListXMLManager"'
    echo 'type       = "XML"'
    echo 'xlink:href = "./xml/InfillSDStationList.xml" />'
    echo ""

    echo '<configLink'
    echo 'id         = "RandomEngine"'
    echo 'type       = "XML"'
    echo 'xlink:href = "./xml/RandomEngineRegistry.xml" />'
    echo ""
    
    echo '<configLink'
    echo 'id         = "databaseServers"'
    echo 'type       = "XML"'
    echo 'xlink:href = "./xml/databaseServers.xml" />'
    echo ""


    echo '</centralConfig>'
    echo ""
    echo '</bootstrap>'

     
 ) > $bootstrapfile


#### MuonCounterSimulatorUSC ####
## muoncountersimulatorfile="G4MuonCounterSimulator.xml.in"
muoncountersimulatorfile="G4MuonCounterSimulator.xml"
echo "Creating G4MuonCounterSimulator file $muoncountersimulatorfile"
( 
	echo '<?xml version="1.0" encoding="iso-8859-1"?>'
    echo ""

	echo '<G4MuonCounterSimulatorUSC>'
	echo ""
	echo '<verbosity>'
	echo '	<!-- 0= silent, 2= verbose for debug --> '
	echo '	<runVerbosity> 0 </runVerbosity> '
	echo '	<eventVerbosity> 0 </eventVerbosity> '
	echo '	<trackingVerbosity> 0 </trackingVerbosity>' 
	echo '</verbosity>'

	echo ""

  echo '<visualization>'
	echo '	<!-- 1, 2= OpenGL immediate (without/with Motif control)'
	echo '			 3, 4= OpenGL stored (without/with Motif control)'
	echo '			 5   = DAWN'
	echo '			 6   = VRML2'
	echo '	-->'
	echo '	<visDriver> 5 </visDriver> '
  echo '  <geometry> 0 </geometry>'
  echo '  <trajectories> 0 </trajectories>'
  echo '</visualization>'

	echo ""
		
	echo '<run>'
	echo '	<!-- <G4MacroFile> MuonCounterSimG4.mac </G4MacroFile> -->'
	echo ""
	echo '	<fullMode> 1 </fullMode>'	
	echo '	<!-- If fastMode set to 1, use custom optical photon tracking.  This speeds up
       the simulation by about a factor of 5.  If fastMode set to 0, use the 
       native G4 tracking for optical photons. -->'
  echo '	<fastMode> 0 </fastMode>'
	echo '</run>'

	echo ""

	echo '<SaveSimInfo use="yes">'
	echo '	<saveFullInfo> 0 </saveFullInfo>'
	echo '	<OutFileName> SimOutData.root </OutFileName> 		<!-- Name of ROOT output filename -->'
	echo '</SaveSimInfo>'

	echo ""


  echo '<!-- Handle the physics interactions (turns on/off some processes) -->'
  echo '<physicsProcesses> '
	echo '	<Optical> 0 </Optical>'
	echo '	<Scintillation> 0 </Scintillation>'
	echo '	<Cherenkov> 0 </Cherenkov>'
	echo '	<WLS> 0 </WLS>'
	echo '	<Absorption> 0 </Absorption>'
	echo '	<Rayleigh> 0 </Rayleigh>'
	echo '  <Boundary> 0 </Boundary>'
	echo '  <muCapture> 0 </muCapture>'
	echo '</physicsProcesses>'

	echo ""

	echo '<!-- Handle the detector geometry & materials -->'
	echo '<worldMaterial> 3 </worldMaterial> <!-- 1= Vacuum, 2= Air, 3= Malargue soil -->'
	echo ""

	echo '<MuonCounterModule> '
	echo '	<nStrip> 49 </nStrip>  <!-- nStrips per detector plane (BATATA 49) -->'
	echo '	<nPlane> 3 </nPlane>   <!-- nPlanes per muon counter (1 or 3 for BATATA) -->'			 
	echo '	<superplaneThickness unit="mm"> 3.0 </superplaneThickness>'
	echo '	<superplaneHeight unit="cm"> 6.0 </superplaneHeight>'
	echo '	<superplaneCasingMaterial> 3 </superplaneCasingMaterial> <!-- possible choices: Vacuum, PVC, GlassFiber, Air -->'
	echo '	<superplaneDistance unit="cm"> '
	echo '		50.0'
	echo '		30.0'
	echo '		200.0'
  echo '	</superplaneDistance>'
	echo '	<enableYplane> 1 </enableYplane>'
	echo '	<planeXYDistance unit="cm"> 0.0 </planeXYDistance>'
	echo '	<planeXYTiltAngle unit="degree"> 90.0 </planeXYTiltAngle>'
	echo '  <surfaceSeparationGap unit="nanometer"> 1.0 </surfaceSeparationGap>'
	echo '</MuonCounterModule>'

	echo ""

	echo '<ScintillatorStrip> '
	echo '	<stripSizeX unit="cm"> 200.0 </stripSizeX>'
	echo '	<stripSizeY unit="cm"> 4.0 </stripSizeY>'
	echo '	<stripSizeZ unit="cm"> 1.0 </stripSizeZ>'
			
	echo '	<stripGrooveX unit="cm"> 200.0 </stripGrooveX>'
	echo '	<stripGrooveY unit="cm"> 0.17 </stripGrooveY>'
	echo '	<stripGrooveZ unit="cm"> 0.17 </stripGrooveZ>'

	echo '	<stripHousingThickness unit="mm"> 0.25 </stripHousingThickness>'
	echo '	<stripHousingUseSpectrumReflectivity> 0 </stripHousingUseSpectrumReflectivity>'
	echo '	<stripHousingReflectivity> 0.90 </stripHousingReflectivity> '
	echo '	<stripHousingSurfaceType> 1 </stripHousingSurfaceType> <!-- 1=polished, 2=ground -->	'

	echo '	<scintillationYield> 11136 </scintillationYield> <!-- in photons/MeV -->	'
	echo '	<scintillationWeight> 1 </scintillationWeight> <!-- true yield ==> w=1.0 -->'
	
	echo ""	
	echo '	<!-- Readout mode '
	echo '			1 => pmt readout at one edge'
	echo '			2 => pmt readout at both edges '
	echo '			3 => pmt readout only fiber at one edge '
	echo '	 		4 => pmt readout only fiber at both edge '
	echo '	-->'
	echo '	<stripReadoutMode> 3 </stripReadoutMode> '
	echo ""
	echo '	<!-- Design mode'
	echo '			1 => only scintillator'
	echo '			2 => scintillator + groove'
	echo '			3 => scintillator + groove + fiber'
	echo '	-->'
	echo '	<stripDesignMode> 3 </stripDesignMode>'

	echo ""
	echo '	<!-- Optical coupling mode'
	echo '			0 => no coupling medium'
	echo '			1 => optical coupling with Air'
	echo '			2 => optical coupling with Silicon Grease'
	echo '	-->'
	echo '	<stripOpticalCouplingMode> 0 </stripOpticalCouplingMode>'

	echo ""
	echo '	<stripEnergyThreshold unit="MeV"> 0.3 </stripEnergyThreshold>'
	echo '	<stripVetoTime unit="ns"> 20.0 </stripVetoTime>'
	echo '	<stripDecayTime unit="ns"> 1.0 </stripDecayTime>	'	

	echo '</ScintillatorStrip>'

	echo ""

	echo '<WLSFiber> '
	echo '	<fiberLength unit="cm"> 200.0 </fiberLength>'
	echo '	<fiberRadius unit="mm"> 0.75 </fiberRadius>'
	echo '	<fiberCaptureLength unit="mm"> 4.8 </fiberCaptureLength>'
	echo '	<fiberAttLength unit="m"> 3.5 </fiberAttLength>'
	echo '	<fiberDecayTime unit="ns"> 6.5 </fiberDecayTime>'
	echo '	<fiberCoreRefractiveIndex> 1.60 </fiberCoreRefractiveIndex>'
	echo '	<fiberClad1RefractiveIndex> 1.49 </fiberClad1RefractiveIndex>'
	echo '	<fiberClad2RefractiveIndex> 1.42 </fiberClad2RefractiveIndex>'

	echo ""
	echo '	<!-- Optical coupling mode'
	echo '			0 => no coupling medium (Vacuum)'
	echo '			1 => optical coupling with Air'
	echo '			2 => optical coupling with Silicon Grease'
	echo '			3 => optical coupling with Epoxy glue'
	echo '	-->'
	echo '	<fiberOpticalCouplingMode> 3 </fiberOpticalCouplingMode>'
	echo '</WLSFiber> '

	echo ""
	echo '<PMT> '
	echo '	<pmtSizeX unit="cm"> 1.0 </pmtSizeX>'
	echo '	<pmtSizeY unit="cm"> 4.0 </pmtSizeY>'
	echo '	<pmtSizeZ unit="cm"> 1.0 </pmtSizeZ>'

	echo '	<photocathodeSizeX unit="mm"> 1.0 </photocathodeSizeX>'
	echo '	<photocathodeSizeY unit="mm"> 40.0 </photocathodeSizeY>'
	echo '	<photocathodeSizeZ unit="mm"> 10.0 </photocathodeSizeZ>'
		
	echo '	<optCouplingSizeX unit="mm"> 0.5 </optCouplingSizeX>'
	echo '	<optCouplingSizeY unit="mm"> 40.0 </optCouplingSizeY>'
	echo '	<optCouplingSizeZ unit="mm"> 10.0 </optCouplingSizeZ>'

	echo '	<useSpectrumQE> 0 </useSpectrumQE>'

	echo '	<averageTransitTime unit="ns"> 12.0 </averageTransitTime>'
	echo '	<spreadTransitTime unit="ns"> 1.85 </spreadTransitTime>'
	echo '	<peYield> 5.0 </peYield> <!-- average nPE produced per MeV of energy deposit (tuned in lab) -->'
	echo '</PMT>'
	
	echo ""
 	
	echo '</G4MuonCounterSimulatorUSC>'

) > $muoncountersimulatorfile


#### ShowerRegenerator ####
## showerregeneratorfile="ShowerRegenerator.xml.in"
showerregeneratorfile="ShowerRegenerator.xml"
echo "Creating ShowerRegenerator file $showerregeneratorfile"
( 
	echo '<?xml version="1.0" encoding="iso-8859-1"?>'
    echo ""

	echo '<ShowerRegenerator>'
	
	echo ""
	echo '<LimitParticlesPerCycle use="yes"> 500000 </LimitParticlesPerCycle>'

  echo '<!-- ' 
  echo '  Cuts on inner and outer radii for particle regeneration.  If the attribute'
  echo '  use="no", the cut is not applied, but note that there may still be inner and outer'
  echo '  radii cuts imposed by the original generator-level shower simulation program'
  echo '  (eg. Aires or Corsika).  Information on generator-level radii cuts can be'
  echo '  extracted from evt::ShowerSimData::GetMinRadiusCut() and '
  echo '  evt::ShowerSimData::GetMaxRadiusCut().'
  echo ' -->'
  echo '<DistanceCuts>'
  echo '  <InnerRadiusCut unit="m"  use="no">  250.0 </InnerRadiusCut>'
  echo '  <OuterRadiusCut unit="km" use="yes"> 100.0 </OuterRadiusCut>'
  echo '</DistanceCuts>'
  
	echo ""

  echo '<!-- Particle energy cuts -->'
  echo '<EnergyCuts>'
  echo '  <ElectronEnergyCut unit="MeV"> 1.0  </ElectronEnergyCut> <!-- 1.0 MeV tanks -->'
  echo '  <MuonEnergyCut     unit="MeV"> 10.0 </MuonEnergyCut> <!-- 10.0 MeV tanks -->'
  echo '  <PhotonEnergyCut   unit="MeV"> 1.0  </PhotonEnergyCut> <!-- 1.0 MeV tanks -->'
  echo '  <HadronEnergyCut   unit="MeV"> 10.0 </HadronEnergyCut> <!-- 10.0 MeV tanks -->'
  echo '  <MesonEnergyCut    unit="MeV"> 10.0 </MesonEnergyCut> <!-- 10.0 MeV tanks -->'
  echo '</EnergyCuts>'

	echo ""
	echo '<!-- Skip particles (mainly for debugging) -->'
	echo '<ParticleSkip>'
  echo '  <SkipElectrons> 0 </SkipElectrons>'
	echo '	<SkipMuons> 0 </SkipMuons>'
	echo '	<SkipPhotons> 0 </SkipPhotons>'
	echo '	<SkipHadrons> 0 </SkipHadrons>'
	echo '	<SkipMesons> 0 </SkipMesons>'
  echo '</ParticleSkip>'
	
	echo ""
  echo '<!-- Algorithm parameters -->'
  echo '<AlgorithmParameters>'
  echo '  <DeltaROverR> 0.1 </DeltaROverR> <!-- dimensionless (dR/R) -->'
  echo '  <DeltaPhi unit="radian"> 0.15 </DeltaPhi>'

	echo ""
	echo '	<!-- This defines the detector area at ground used for resampling in the case of muon counters -->'
	echo '	<SamplingDetAreaSizeX unit="m"> 2. </SamplingDetAreaSizeX> '
 	echo '	<SamplingDetAreaSizeY unit="m"> 2. </SamplingDetAreaSizeY> '
 
	echo ""

	echo '  <HorizontalParticleCut> 1e-3 </HorizontalParticleCut>'
  echo '  <UseStationPositionMatrix> 1 </UseStationPositionMatrix>'
  echo ' <PhiGranularity> 2 </PhiGranularity>'
  echo '  <RGranularity> 2 </RGranularity>'
  echo '  <!-- set to 0 to disable time smearing -->'
  echo '  <LogGaussSmearingWidth> 0.1 </LogGaussSmearingWidth>'
  echo '</AlgorithmParameters>'

	echo ""

  echo '<!--'
  echo '  If ResamplingWeightLimiting is switched on, a special'
  echo '  algorithm is used to handle particles with very large'
  echo '  weights.  The algorithm is described by the following '
  echo '  pseudo-code, where the values WeightCounter and '
  echo '  AccumulatedWeight are defined in this XML file.'
 	echo '  ATTENTION: at the moment this ONLY works for the '
  echo '  G4FastTank simulation!  (as of 10 Oct 2008)'

	echo ""

  echo '  weight_counter = 0'
  echo '  if (particle_weight > WeightThreshold) {'
  echo '    counter += weight'
  echo '    if (weight_counter < AccumulatedWeightLimit) {'
  echo '      resample and inject n particles with unity weight'
  echo '   } else {'
  echo '      inject 1 particle with weight particle_weight '
  echo '    }'
  echo '  } else {'
  echo '    resample and inject n particles with unity weight'
  echo '    where n is from Poissonian distristribution with'
  echo '    mean equal to particle_weight'
  echo '  }'
  echo ' -->'

	echo ""
  echo '<ResamplingWeightLimiting use="no">'
  echo '  <WeightThreshold> 500 </WeightThreshold> '
  echo '  <AccumulatedWeightLimit> 100000 </AccumulatedWeightLimit> '
  echo '</ResamplingWeightLimiting>'

	echo ""
	echo '<SaveInjParticlesInfo use="yes">'
	echo '	<OutFileName> InjParticle.root </OutFileName> 		<!-- Name of ROOT output filename -->'
	echo '</SaveInjParticlesInfo>'
	
	echo ""
	echo '</ShowerRegenerator>'

) > $showerregeneratorfile


#### EventFileRecorder ####
##eventfilerecorder="EventFileRecorder.xml.in"
eventfilerecorder="EventFileRecorder.xml"
echo "Creating EventFileRecorder file $eventfilerecorder"
( 
	echo '<?xml version="1.0" encoding="iso-8859-1"?>'
    echo ""

	echo '<EventFileRecorder>'
	
	echo ""

	echo '<!-- Turn on/off the storing of gen/det/sim/rec info -->'
	echo '<SaveInfo>'
  echo '  <GenInfo> 1 </GenInfo>                      <!-- on/off generated info -->'
	echo '	<DetInfo> 1 </DetInfo>                      <!-- on/off detector info -->'
	echo '	<SimInfo> 1 </SimInfo>                      <!-- on/off simulation info -->'
	echo '	<RecInfo> 1 </RecInfo>                      <!-- on/off reconstruction info -->'
	echo '	<OutFileName> Output.root </OutFileName> 		<!-- Name of ROOT output filename -->'
  echo '</SaveInfo>'
	
	echo ""

	echo '</EventFileRecorder>'

) > $eventfilerecorder


#### G4MuonCounterParticleInjector ####
particleinjector="G4MuonCounterParticleInjector.xml"
echo "Creating G4MuonCounterParticleInjector file $particleinjector"
( 
	echo '<?xml version="1.0" encoding="iso-8859-1"?>'
    echo ""


	echo '<G4MuonCounterParticleInjector>'
	echo ""
  
	echo '<config>'
  echo '  <UseSingleTank> 1 </UseSingleTank> <!-- Inject in 1 tank only -->'
  echo '  <UseInjectAllParticles> 0 </UseInjectAllParticles>'
  echo '  <UseSinglePosition> 0 </UseSinglePosition>'
	echo '	<UseRandomPositionXDistributionFromFile> 0 </UseRandomPositionXDistributionFromFile>'
	echo '	<UseRandomPositionYDistributionFromFile> 0 </UseRandomPositionYDistributionFromFile>'
  echo '  <UseDiscreteDirection> 0 </UseDiscreteDirection>'
	echo '	<UseRandomAzimuthDistributionFromFile> 0 </UseRandomAzimuthDistributionFromFile>'
	echo '	<UseRandomZenithDistributionFromFile> 1 </UseRandomZenithDistributionFromFile>'
  echo '  <UseDiscreteEnergy> 0 </UseDiscreteEnergy>'
	echo '	<UseRandomEnergyDistributionFromFile> 1 </UseRandomEnergyDistributionFromFile>'
  echo '  <UseDiscreteTime> 1 </UseDiscreteTime>'
	echo '	<randomDistributionFile> EASRandomDistribution.root </randomDistributionFile>'
  echo '</config>'


	echo ""

  echo '<!-- Number of particles per tank -->'
  echo '<NumberOfParticles> 1000 </NumberOfParticles>'

  echo '<!-- tank ID if using single tank option -->'
  echo '<SingleTankID> 78 </SingleTankID>'

  echo '<!-- Mixture of photons, e+/e-, and mu+/mu- in tank only if UseSpectrum set -->'
	echo '<!--'
	echo '	eElectron = 11, ePositron = -11 '
	echo '  ePhoton = 22 '
	echo '	eMuon = 13, eAntiMuon = -13'
	echo '	eNeutron = 2112, eProton = 2212'
	echo '-->'
  echo '<ParticleType> 13 </ParticleType>' 

	echo ""

  echo '<UseDiscreteTimeSpectrum>'
  echo '  <ParticleTime unit="ns"> 500.0 </ParticleTime>'
  echo '</UseDiscreteTimeSpectrum>'

	echo ""
  echo '<UseContinuousTimeSpectrum>'
  echo '  <!-- To be completed -->'
  echo '</UseContinuousTimeSpectrum>'

	echo ""
  echo '<!-- Inject particle in single position -->'
  echo '<SinglePosition>'
  echo '  <ParticleX unit="cm"> 0.0 </ParticleX>'
  echo '  <ParticleY unit="cm"> 0.0 </ParticleY>'
  echo '  <ParticleZ unit="cm"> 0.0 </ParticleZ>'
  echo '</SinglePosition> <!-- spec. entry point if set-->'

	echo ""
	echo '<RandomPosition>'
	echo '	<RandomAroundPosition> 0 </RandomAroundPosition>'
	echo '	<ParticleX unit="cm"> 0.0 </ParticleX>'
  echo '  <ParticleY unit="cm"> 0.0 </ParticleY>'
  echo '  <ParticleZ unit="cm"> 0.0 </ParticleZ>'
  echo '  <GridX unit="cm"> 200.0 </GridX>'
  echo '  <GridY unit="cm"> 200.0 </GridY>'
  echo '  <GridZ unit="cm"> 0.0 </GridZ>'
  echo '</RandomPosition> <!-- spec. entry point if set-->'

	echo ""

  echo '<!-- Inject particle with continuous zenith and azimuth distribution(radians) -->'
  echo '<UseContinuousDirectionSpectrum>'
  echo '  <Zenith> cos(x)*cos(x)*sin(x) </Zenith>'
	echo '	<ZenithMin unit="degree"> 0 </ZenithMin>'
	echo '	<ZenithMax unit="degree"> 60 </ZenithMax>'

  echo '  <Azimuth> 1 </Azimuth>'
	echo '	<AzimuthMin unit="degree"> 0 </AzimuthMin>'
	echo '	<AzimuthMax unit="degree"> 180 </AzimuthMax>'
  echo '</UseContinuousDirectionSpectrum>'

	echo ""

  echo '<!-- Inject particle with discrete zenith and azimuth distribution -->'
  echo '<UseDiscreteDirectionSpectrum>'
  echo '  <Zenith unit="degree">  0 </Zenith>'
  echo '  <Azimuth unit="degree"> 0 </Azimuth>'
  echo '</UseDiscreteDirectionSpectrum>'

	echo ""

  echo '<!-- Inject particle with continuous energy distribution -->'
  echo '<UseContinuousEnergySpectrum>'
  echo '  <MuonSpectrum> x </MuonSpectrum>'
  echo '  <MuonEnergyMin unit="GeV"> 0.02 </MuonEnergyMin>'
  echo '  <MuonEnergyMax unit="GeV"> 3.0 </MuonEnergyMax>'
	echo ""
  echo '  <ElectronSpectrum> 90.0*(pow(x,-2.5)) </ElectronSpectrum>'
  echo '  <ElectronEnergyMin unit="GeV"> 0.02 </ElectronEnergyMin>'
  echo '  <ElectronEnergyMax unit="GeV"> 3.0 </ElectronEnergyMax>'

  echo '  <PhotonSpectrum> 1.3*(pow(x,-1.8)) </PhotonSpectrum>'
  echo '  <PhotonEnergyMin unit="GeV"> 0.02 </PhotonEnergyMin>'
  echo '  <PhotonEnergyMax unit="GeV"> 3.0 </PhotonEnergyMax>'

	echo ""
	echo '	<HadronSpectrum> x </HadronSpectrum>'
  echo '  <HadronEnergyMin unit="GeV"> 0.02 </HadronEnergyMin>'
  echo '  <HadronEnergyMax unit="GeV"> 3.0 </HadronEnergyMax>'
  echo '</UseContinuousEnergySpectrum> '

	echo ""

  echo '<!-- Inject particle with discrete energy distribution -->'
  echo '<UseDiscreteEnergySpectrum> ' 
  echo '  <MuonSpectrum> 1 </MuonSpectrum>'
  echo '  <MuonEnergy unit="GeV"> 1.0 </MuonEnergy>'
	echo ""
  echo '  <ElectronSpectrum> 1 </ElectronSpectrum>'
  echo '  <ElectronEnergy unit="GeV"> 1.0  </ElectronEnergy>'
	echo ""
  echo '  <PhotonSpectrum> 1 </PhotonSpectrum>'
  echo '  <PhotonEnergy unit="GeV"> 1.0 </PhotonEnergy>'
	echo ""
	echo '	<HadronSpectrum> 1 </HadronSpectrum>'
  echo '  <HadronEnergy unit="GeV"> 1.0 </HadronEnergy>'
  echo '</UseDiscreteEnergySpectrum> '

	echo ""
	echo ""
	echo '</G4MuonCounterParticleInjector>'

) > $particleinjector


#### SD MANAGERS ####
##smanagerfile="SManagerRegisterConfig.xml.in"
smanagerfile="SManagerRegisterConfig.xml"
echo "Creating SManager file $smanagerfile"
( 
    echo '<?xml version="1.0" encoding="iso-8859-1"?>'
    echo ""
    echo '<SDetectorManagerList xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'
    ###echo "xsi:noNamespaceSchemaLocation='@SCHEMALOCATION@/SManagerRegisterConfig.xsd'>"
		echo "xsi:noNamespaceSchemaLocation='""$AUGEROFFLINEROOT/share/auger-offline/config/SManagerRegisterConfig.xsd'>"
    echo ""
    
    #echo '<manager> SStationListSQLManager       </manager>'
		echo '<manager> SStationListXMLManager       </manager>'
    echo '<manager> SModelsXMLManager            </manager>'
    #echo '<manager> T2LifeROOTFileManager        </manager>'
    echo '<manager> EventStationPositionsManager </manager>'
    echo '<manager> SdSimCalibrationManager </manager>'
    echo ""
    echo '</SDetectorManagerList>'

) > $smanagerfile


if [ "$MYSQLSERVER" -eq 1 ]; then
  ### DATABASES SERVER LIST ###
	##dbserverfile="databaseServers.xml.in"
	dbserverfile="databaseServers.xml"
	echo "Creating databaseServers file $dbserverfile"
	( 
    echo '<?xml version="1.0" encoding="iso-8859-1"?>'
    echo ""
    echo '<databaseServers xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'
    ### echo "xsi:noNamespaceSchemaLocation='@SCHEMALOCATION@/databaseServers.xsd'>"
		echo "xsi:noNamespaceSchemaLocation='""$AUGEROFFLINEROOT/share/auger-offline/config/databaseServers.xsd'>"
    echo ""

    echo '<serverList>'
    
    echo ""
    
    echo '<server> <!-- cc01 server -->'
    echo '  <hostName> cc01.ct.infn.it </hostName>'
    echo '  <port> 3306 </port>'
    echo '  <userName> Mocca </userName>'
    echo '  <password> Sibyll </password>'
    echo '</server>'

    echo ""

    echo '<server> <!-- my ctserver -->'
    echo '  <hostName> 192.84.150.137 </hostName>'
    echo '  <port> 3306 </port>'
    echo '  <userName> Mocca </userName>'
    echo '  <password> Sibyll </password>'
    echo '</server>'

    echo ""

    echo '<server> <!-- other ctserver -->'
    echo '  <hostName> 192.167.0.118 </hostName>'
    echo '  <port> 3306 </port>'
    echo '  <userName> Mocca </userName>'
    echo '  <password> Sibyll </password>'
    echo '</server>'

    echo ""

    echo '<server> <!-- augergs server -->'
    echo '  <hostName> 192.168.0.3 </hostName>'
    echo '  <port> 3306 </port>'
    echo '  <userName> Mocca </userName>'
    echo '  <password> Sibyll </password>'
    echo '</server>'

    echo ""


    echo '<server> <!-- Northeastern mirror -->'
    echo '  <hostName> 129.10.132.228 </hostName>'
    echo '  <port> 3306 </port>'
    echo '  <userName> Mocca </userName>'
    echo '  <password> Sibyll </password>'
    echo '</server>'

    echo ""

    echo '<server> <!-- NYU mirror -->'
    echo '  <hostName> offline.physics.nyu.edu </hostName>'
    echo '  <port> 3306 </port>'
    echo '  <userName> Mocca </userName>'
    echo '  <password> Sibyll </password>'
    echo '</server>'
    
    echo ""

    echo '<server> <!-- University of Nova Gorica mirror -->'
    echo '  <hostName> offline.p-ng.si </hostName>'
    echo '  <port> 3306 </port>'
    echo '  <userName> Mocca </userName>'
    echo '  <password> Sibyll </password>'
    echo '</server>'

		echo '<server> <!-- University of Nova Gorica mirror 2 -->'
    echo '  <hostName> offline2.p-ng.si </hostName>'
    echo '  <port> 3306 </port>'
    echo '  <userName> Mocca </userName>'
    echo '  <password> Sibyll </password>'
    echo '</server>'

		echo '<server> <!-- University of Nova Gorica mirror 3 -->'
    echo '  <hostName> offline3.p-ng.si </hostName>'
    echo '  <port> 3306 </port>'
    echo '  <userName> Mocca </userName>'
    echo '  <password> Sibyll </password>'
    echo '</server>'
    
    echo ""

    echo '</serverList>'
  
    echo '</databaseServers>'

) > $dbserverfile

elif [ "$MYSQLSERVER" -eq 2 ]; then
  ### DATABASES SERVER LIST ###
  ##dbserverfile="databaseServers.xml.in"
	dbserverfile="databaseServers.xml"
  echo "Creating databaseServers file $dbserverfile"
  ( 
    echo '<?xml version="1.0" encoding="iso-8859-1"?>'
    echo ""
    echo '<databaseServers xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'
    ###echo "xsi:noNamespaceSchemaLocation='@SCHEMALOCATION@/databaseServers.xsd'>"
		echo "xsi:noNamespaceSchemaLocation='""$AUGEROFFLINEROOT/share/auger-offline/config/databaseServers.xsd'>"
    echo ""

    echo '<serverList>'
    
    echo ""
    
    echo '<server> <!-- my ctserver -->'
    echo '  <hostName> 192.84.150.137 </hostName>'
    echo '  <port> 3306 </port>'
    echo '  <userName> Mocca </userName>'
    echo '  <password> Sibyll </password>'
    echo '</server>'

    echo ""

    echo '<server> <!-- other ctserver -->'
    echo '  <hostName> 192.167.0.118 </hostName>'
    echo '  <port> 3306 </port>'
    echo '  <userName> Mocca </userName>'
    echo '  <password> Sibyll </password>'
    echo '</server>'

    echo ""

    echo '<server> <!-- cc01 server -->'
    echo '  <hostName> cc01.ct.infn.it </hostName>'
    echo '  <port> 3306 </port>'
    echo '  <userName> Mocca </userName>'
    echo '  <password> Sibyll </password>'
    echo '</server>'

    echo ""

    echo '<server> <!-- augergs server -->'
    echo '  <hostName> 192.168.0.3 </hostName>'
    echo '  <port> 3306 </port>'
    echo '  <userName> Mocca </userName>'
    echo '  <password> Sibyll </password>'
    echo '</server>'

    echo ""

    echo '<server> <!-- Northeastern mirror -->'
    echo '  <hostName> 129.10.132.228 </hostName>'
    echo '  <port> 3306 </port>'
    echo '  <userName> Mocca </userName>'
    echo '  <password> Sibyll </password>'
    echo '</server>'

    echo ""

    echo '<server> <!-- NYU mirror -->'
    echo '  <hostName> offline.physics.nyu.edu </hostName>'
    echo '  <port> 3306 </port>'
    echo '  <userName> Mocca </userName>'
    echo '  <password> Sibyll </password>'
    echo '</server>'
    
    echo ""

    echo '<server> <!-- University of Nova Gorica mirror -->'
    echo '  <hostName> offline.p-ng.si </hostName>'
    echo '  <port> 3306 </port>'
    echo '  <userName> Mocca </userName>'
    echo '  <password> Sibyll </password>'
    echo '</server>'

		echo '<server> <!-- University of Nova Gorica mirror 2 -->'
    echo '  <hostName> offline2.p-ng.si </hostName>'
    echo '  <port> 3306 </port>'
    echo '  <userName> Mocca </userName>'
    echo '  <password> Sibyll </password>'
    echo '</server>'

		echo '<server> <!-- University of Nova Gorica mirror 3 -->'
    echo '  <hostName> offline3.p-ng.si </hostName>'
    echo '  <port> 3306 </port>'
    echo '  <userName> Mocca </userName>'
    echo '  <password> Sibyll </password>'
    echo '</server>'
    
    echo ""

    echo '</serverList>'
  
    echo '</databaseServers>'

) > $dbserverfile

elif [ "$MYSQLSERVER" -eq 3 ]; then
   ### DATABASES SERVER LIST ###
   ##dbserverfile="databaseServers.xml.in"
	 dbserverfile="databaseServers.xml"  
   echo "Creating databaseServers file $dbserverfile"
   ( 
    echo '<?xml version="1.0" encoding="iso-8859-1"?>'
    echo ""
    echo '<databaseServers xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'
    ### echo "xsi:noNamespaceSchemaLocation='@SCHEMALOCATION@/databaseServers.xsd'>"
		echo "xsi:noNamespaceSchemaLocation='""$AUGEROFFLINEROOT/share/auger-offline/config/databaseServers.xsd'>"
    echo ""

    echo '<serverList>'
    
    echo ""

    echo '<server> <!-- other ctserver -->'
    echo '  <hostName> 192.167.0.118 </hostName>'
    echo '  <port> 3306 </port>'
    echo '  <userName> Mocca </userName>'
    echo '  <password> Sibyll </password>'
    echo '</server>'

    echo ""

    echo '<server> <!-- my ctserver -->'
    echo '  <hostName> 192.84.150.137 </hostName>'
    echo '  <port> 3306 </port>'
    echo '  <userName> Mocca </userName>'
    echo '  <password> Sibyll </password>'
    echo '</server>'

    echo ""

    echo '<server> <!-- cc01 server -->'
    echo '  <hostName> cc01.ct.infn.it </hostName>'
    echo '  <port> 3306 </port>'
    echo '  <userName> Mocca </userName>'
    echo '  <password> Sibyll </password>'
    echo '</server>'

    echo ""
    echo '<server> <!-- augergs server -->'
    echo '  <hostName> 192.168.0.3 </hostName>'
    echo '  <port> 3306 </port>'
    echo '  <userName> Mocca </userName>'
    echo '  <password> Sibyll </password>'
    echo '</server>'

    echo ""

    echo '<server> <!-- Northeastern mirror -->'
    echo '  <hostName> 129.10.132.228 </hostName>'
    echo '  <port> 3306 </port>'
    echo '  <userName> Mocca </userName>'
    echo '  <password> Sibyll </password>'
    echo '</server>'

    echo ""

    echo '<server> <!-- NYU mirror -->'
    echo '  <hostName> offline.physics.nyu.edu </hostName>'
    echo '  <port> 3306 </port>'
    echo '  <userName> Mocca </userName>'
    echo '  <password> Sibyll </password>'
    echo '</server>'
    
    echo ""

    echo '<server> <!-- University of Nova Gorica mirror -->'
    echo '  <hostName> offline.p-ng.si </hostName>'
    echo '  <port> 3306 </port>'
    echo '  <userName> Mocca </userName>'
    echo '  <password> Sibyll </password>'
    echo '</server>'

		echo '<server> <!-- University of Nova Gorica mirror 2 -->'
    echo '  <hostName> offline2.p-ng.si </hostName>'
    echo '  <port> 3306 </port>'
    echo '  <userName> Mocca </userName>'
    echo '  <password> Sibyll </password>'
    echo '</server>'

		echo '<server> <!-- University of Nova Gorica mirror 3 -->'
    echo '  <hostName> offline3.p-ng.si </hostName>'
    echo '  <port> 3306 </port>'
    echo '  <userName> Mocca </userName>'
    echo '  <password> Sibyll </password>'
    echo '</server>'
    
    echo ""

    echo '</serverList>'
  
    echo '</databaseServers>'

) > $dbserverfile


else
    echo "Error in MySQL server flagging"
    exit 0
fi





#### OTHER MODULE OVERRIDDEN XMLS (Reading,Generation,UserModule,...) #### 

myeventgeneratorfile="EventGenerator.xml"
echo "Creating EventGenerator file $myeventgeneratorfile"
( 
    echo '<?xml version="1.0" encoding="iso-8859-1"?>'
    echo ""

    echo '<!-- Configuration of Module EventGenerator -->'
    echo ""
    
    echo '<EventGenerator>'
    echo "" 

    echo '<!-- Specify what you are planning to simulate: SD, FD, Hy -->'
    echo '<mode> SD </mode>'

    echo ""
    echo '<!-- set the event identifier -->'
    echo '<eventIdentifier>'
    echo '<libraryIdentifier> Test </libraryIdentifier>'
    echo '<format> Library_%1%:Run_%2%:Shower_%3%:Use_%4% </format>'
    echo '</eventIdentifier>'

    echo ""

    echo '<!-- Specify how to randomize the core position -->'
    echo '<coreRandomization>'
    echo ""
    echo '<centerOfTile>'
		echo '	<stationAtCenter> 78 </stationAtCenter> '
    echo '</centerOfTile>'

    echo ""
    echo '<sizeOfTile>  <!-- not alligned with grid.. needs to be fixed -->'
    echo '<deltaNorthing unit="meter"> 600. </deltaNorthing>'
    echo '<deltaEasting unit="meter"> 600. </deltaEasting>'
    echo '</sizeOfTile>'
    
    echo ""
    echo '</coreRandomization>'

    echo ""
    echo '<!-- Time stamp of the core impact on ground -->'
    echo '<eventTime> 2010-01-01T04:33:12.5 </eventTime>'

   
    echo ""
    echo '</EventGenerator>'
    

) > $myeventgeneratorfile



##randomengineregistryfile="RandomEngineRegistry.xml.in"
randomengineregistryfile="RandomEngineRegistry.xml"
theDetectorSeed=$RANDOM
thePhysicsSeed=$RANDOM

echo "Creating RandomEngineRegistry file $randomengineregistryfile"
( 
    echo '<?xml version="1.0" encoding="iso-8859-1"?>'
    echo ""

    echo '<!-- Configuration for RandomEngineRegistry -->'
    echo ""
    
    echo '<RandomEngineRegistry xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'
    ###echo "xsi:noNamespaceSchemaLocation='@SCHEMALOCATION@/RandomEngineRegistry.xsd'>"
		echo "xsi:noNamespaceSchemaLocation='""$AUGEROFFLINEROOT/share/auger-offline/config/RandomEngineRegistry.xsd'>"

    echo ""
    echo '<!-- The initial seed for the eDetector random engine -->'
    echo "<DetectorSeed> $theDetectorSeed </DetectorSeed>"

    echo ""
    echo '<!-- The initial seed for the ePhysics random engine -->'
    echo "<PhysicsSeed> $thePhysicsSeed </PhysicsSeed>"

    echo ""
    echo '</RandomEngineRegistry>'
     

) > $randomengineregistryfile


##eventfilereaderfile="EventFileReader.xml.in"
eventfilereaderfile="EventFileReader.xml"
echo "Creating EventFileReader file $eventfilereaderfile"
( 
    echo '<?xml version="1.0" encoding="iso-8859-1"?>'
    echo ""
    
    echo '<!-- Configuration of Module EventFileReader -->'
    echo ""

    echo '<EventFileReader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'
    ###echo "xsi:noNamespaceSchemaLocation='@SCHEMALOCATION@/EventFileReader.xsd'>"
		echo "xsi:noNamespaceSchemaLocation='""$AUGEROFFLINEROOT/share/auger-offline/config/EventFileReader.xsd'>"

    echo ""

    echo '<!-- file type  Offline native'
    echo '                FDAS'
    echo '                CDAS'
    echo '                CORSIKA'
    echo '                AIRES -->'

    echo ""
    echo '<InputFileType> CORSIKA </InputFileType>'

    echo ""
    echo '<!-- mode : 0 for reading (implies event is cleared), 1 for merging -->'
    echo '<InputMode> 0 </InputMode>'

    echo ""
    echo "<InputFilenames> /data1/Showers/Corsika/Vertical/Proton/$SIMFILENAME </InputFilenames>"
		##echo "<InputFilenames> ../trunk/data/$SIMFILENAME </InputFilenames>"

    echo ""
    echo '</EventFileReader>'
   

) > $eventfilereaderfile


modulesequencefile="ModuleSequence.xml"
echo "Creating ModuleSequence file $modulesequencefile"
( 
    
    echo '<!-- A sequence for an FD simulation -->'
    echo '<sequenceFile>'
    echo ""

    echo '<enableTiming/>'
    echo ""

    echo '<moduleControl>'
    echo ""

    echo '<loop numTimes="'"$NEV"'"' 'pushEventToStack="yes">'
    echo ""
    echo '  <module> EventFileReaderOG            </module>'
		echo ""
		echo '<loop numTimes="1" pushEventToStack="yes">'
		echo ""
    echo '  	<module> EventGeneratorOG             </module>'
    echo ""

		echo '<!-- ============== MuonCounter simulation part =====================  -->'
		echo '		<loop numTimes="unbounded" pushEventToStack="no">'
    echo '      <module> ShowerRegenerator </module>'
    echo '      <module> G4MuonCounterSimulatorUSC  </module> '
    echo '		</loop>'

		echo ""

		echo '<!-- ============== MuonCounter simulation part (particle injector) =====================  -->'
		echo '<!--  <module> G4MuonCounterParticleInjector </module> -->'
    echo '<!--  <module> G4MuonCounterSimulatorUSC  </module> -->'
    
		echo ""

    echo '<!-- ============== MuonCounter reconstruction part =====================  -->'
    echo '<!-- <module> G4MuonCounterReconstructorUSC                </module> -->'
    echo '<!-- ======================================================== -->'

    echo ""
  
    echo ""
    
    echo '		<module> EventFileRecorder     </module>'
    echo '	</loop>'
    echo ""
    echo '</loop>'
    
    echo ""
    echo '</moduleControl>'
    
    echo ""
    echo '</sequenceFile>'
   

) > $modulesequencefile




