#!/bin/sh



function GetCPUUsage() {
                    
	local CPUId=$1
	local MonitoringTime=$2

	local CPUPercent=-1

	#####################################
	####   START
	#####################################
	## print command output
	echo "*** Top CPU-consuming processes ***"
	ps -eo pcpu,pid,user,args | sort -k 1 -r | head -10

	echo ""
	echo ""

	## print command output
	echo "*** CPU-usage list ***"
	cat /proc/stat | grep cpu$CPUId

	echo ""
	echo ""

	local cpu=`cat /proc/stat | grep cpu$CPUId | awk '{print $1}'`
	local totalUser_start=`cat /proc/stat| grep cpu$CPUId | awk '{print $2}'`
	local totalUserLow_start=`cat /proc/stat| grep cpu$CPUId | awk '{print $3}'`
	local totalSys_start=`cat /proc/stat| grep cpu$CPUId | awk '{print $4}'`
	local totalIdle_start=`cat /proc/stat| grep cpu$CPUId | awk '{print $5}'`
	
	## sleep a MonitoringTime and then calculate again load
	sleepCommand="sleep $MonitoringTime"'s'
	echo $sleepCommand
	eval $sleepCommand

	#####################################
	####   END
	#####################################
	## print command output
	echo "*** Top CPU-consuming processes ***"
	ps -eo pcpu,pid,user,args | sort -k 1 -r | head -10

	echo ""
	echo ""

	## print command output
	echo "*** CPU-usage list ***"
	cat /proc/stat | grep cpu$CPUId

	echo ""
	echo ""
	
	local totalUser_end=`cat /proc/stat| grep cpu$CPUId | awk '{print $2}'`
	local totalUserLow_end=`cat /proc/stat| grep cpu$CPUId | awk '{print $3}'`
	local totalSys_end=`cat /proc/stat| grep cpu$CPUId | awk '{print $4}'`
	local totalIdle_end=`cat /proc/stat| grep cpu$CPUId | awk '{print $5}'`


	if [ $totalUser_end -lt $totalUser_start ] || [ $totalUserLow_end -lt $totalUserLow_start ] || [ $totalSys_end -lt $totalSys_start ] || [ $totalIdle_end -lt $totalIdle_start ]; then
		## Overflow detection. Just skip this value.
    CPUPercent=-1;
	else
		## Calculate CPU usage
		(( usage= $totalUser_end - $totalUser_start + $totalUserLow_end - $totalUserLow_start + $totalSys_end - $totalSys_start ))
		(( total= $usage + $totalIdle_end - $totalIdle_start ))
		(( CPUPercent= 100 * $usage / $total ))
	fi

	echo $CPUPercent;
}
# close function



CPUId=$1
MonitoringTime=$2

echo "Monitoring CPU $CPUId on a $MonitoringTime sec time inverval" 
echo ""

CPULoad=$(GetCPUUsage $CPUId $MonitoringTime) 
echo "CPU$CPUId Load= $CPULoad%"



