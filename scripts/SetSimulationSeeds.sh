#/bin/sh

NanoSec=`date -u +%N`
FirstSeed=$RANDOM
SecondSeed=$RANDOM
CurrentPhysicsSeed=$((FirstSeed+NanoSec))
CurrentDetectorSeed=$((SecondSeed+NanoSec))

echo "Seed for the simulation --> $CurrentPhysicsSeed $CurrentDetectorSeed"

echo "Creating RandomEngineRegistry file RandomEngineRegistry.xml"
(
    echo '<?xml version="1.0" encoding="iso-8859-1"?>'
    echo ""
    
    echo '<!-- Configuration for RandomEngineRegistry -->'
    echo ""

    echo '<RandomEngine xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'
    echo "xsi:noNamespaceSchemaLocation='@SCHEMALOCATION@/RandomEngine.xsd'>"
 
    echo ""
    echo '<!-- The initial seed for the eDetector random engine -->'
    echo "<DetectorSeed> $CurrentDetectorSeed </DetectorSeed>"
    
    echo ""
    echo '<!-- The initial seed for the ePhysics random engine -->'
    echo "<PhysicsSeed> $CurrentPhysicsSeed </PhysicsSeed>"
    
    echo ""
    echo '</RandomEngine>'
    
    
) > RandomEngineRegistry.xml


